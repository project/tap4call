<?php

namespace Drupal\rasalas;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RasalasSettings.
 *
 * @package Drupal\rasalas
 */
class RasalasSettings implements RasalasSettingsInterface, ContainerInjectionInterface
{

    const RASALAS_PAGES = "/admin\n/admin/*\n/batch\n/node/add*\n/node/*/*\n/user/*/*";

    /**
     * Rasalas config.
     *
     * @var \Drupal\Core\Config\ImmutableConfig
     */
    protected $config;

    /**
     * Settings.
     *
     * @var array
     */
    protected $settings;

    /**
     * RasalasSettings constructor.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
     *   Config factory.
     */
    public function __construct(
        ConfigFactoryInterface $configFactory
    )
    {
        $this->config = $configFactory->get('rasalas.settings');
        $this->getSettings();
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getSettings()
    {
        if (!$this->settings) {
            $settings = (array)$this->config->getOriginal();
            $settings += [
                'script_id' => NULL,
                'visibility_pages' => 0,
                'pages' => static::RASALAS_PAGES,
                'visibility_roles' => 0,
                'roles' => [],
            ];

            $this->settings = $settings;
        }
        return $this->settings;
    }

    /**
     * {@inheritdoc}
     */
    public function getSetting($key, $default = NULL)
    {
        $this->getSettings();
        return array_key_exists($key, $this->settings) ? $this->settings[$key] : $default;
    }

}
