<?php

namespace Drupal\rasalas;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Asset\AssetCollectionOptimizerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Class SnippetBuilder.
 *
 * @package Drupal\rasalas
 */
class SnippetBuilder implements SnippetBuilderInterface, ContainerInjectionInterface
{
    /**
     * Rasalas settings.
     *
     * @var \Drupal\rasalas\RasalasSettingsInterface
     */
    protected $settings;

    /**
     * SnippetBuilder constructor.
     *
     * @param \Drupal\rasalas\RasalasSettingsInterface $settings
     *   Rasalas settings.
     */
    public function __construct(
        StateInterface $state,
        ConfigFactoryInterface $config_factory,
        RasalasSettingsInterface $settings,
        ModuleHandlerInterface $module_handler,
        AssetCollectionOptimizerInterface $js_collection_optimizer,
        MessengerInterface $messenger
    )
    {
        $this->settings = $settings;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('rasalas.settings')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function pageAttachment(array &$attachments)
    {
        $attachments['#attached']['html_head'][] = [
            [
                '#type' => 'html_tag',
                '#tag' => 'script',
                '#attributes' => [
                    'src' => 'https://panel.tap4call.com/widget/tap4call.js',
                    'id'=> $this->settings->getSetting('script_id')
                ],
            ],
            'rasalas_script_tag',
        ];
    }
}
