<?php

namespace Drupal\rasalas\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rasalas\RasalasSettingsInterface;
use Drupal\rasalas\SnippetBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Rasalas settings for this site.
 */
class RasalasAdminSettingsForm extends ConfigFormBase
{

    /**
     * Rasalas settings.
     *
     * @var \Drupal\rasalas\RasalasSettingsInterface
     */
    protected $rasalasSettings;

    /**
     * Snippet builder.
     *
     * @var \Drupal\rasalas\SnippetBuilderInterface
     */
    protected $snippetBuilder;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        ConfigFactoryInterface $config_factory,
        RasalasSettingsInterface $rasalas_settings,
        SnippetBuilderInterface $snippet_builder
    )
    {
        parent::__construct($config_factory);
        $this->rasalasSettings = $rasalas_settings;
        $this->snippetBuilder = $snippet_builder;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory'),
            $container->get('rasalas.settings'),
            $container->get('rasalas.snippet')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'rasalas_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return ['rasalas.settings'];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['general'] = [
            '#type' => 'details',
            '#title' => $this->t('General settings'),
            '#open' => TRUE,
        ];

        $form['general']['script_id'] = [
            '#default_value' => $this->rasalasSettings->getSetting('script_id'),
            '#description' => $this->t('Proszę podać swój tap4call id.'),
            '#maxlength' => 36,
            '#required' => TRUE,
            '#size' => 36,
            '#title' => $this->t('tap4call id'),
            '#type' => 'textfield',
        ];

        $visibility = $this->rasalasSettings->getSetting('visibility_pages');
        $pages = $this->rasalasSettings->getSetting('pages');

        // Visibility settings.
        $form['association']['page_script'] = [
            '#type' => 'details',
            '#title' => $this->t('Pages'),
            '#group' => 'script_scope',
            '#open' => TRUE,
        ];

        if ($visibility == 2) {
            $form['association']['page_script'] = [];
            $form['association']['page_script']['rasalas_visibility_pages'] = [
                '#type' => 'value',
                '#value' => 2,
            ];
            $form['association']['page_script']['rasalas_pages'] = [
                '#type' => 'value',
                '#value' => $pages,
            ];
        } else {
            $options = [
                $this->t('Every page except the listed pages'),
                $this->t('The listed pages only'),
            ];
            $description_args = [
                '%blog' => 'blog',
                '%blog-wildcard' => 'blog/*',
                '%front' => '<front>',
            ];
            $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", $description_args);
            $title = $this->t('Pages');

            $form['association']['page_script']['rasalas_visibility_pages'] = [
                '#type' => 'radios',
                '#title' => $this->t('Add script to specific pages'),
                '#options' => $options,
                '#default_value' => $visibility,
            ];
            $form['association']['page_script']['rasalas_pages'] = [
                '#type' => 'textarea',
                '#title' => $title,
                '#title_display' => 'invisible',
                '#default_value' => $pages,
                '#description' => $description,
                '#rows' => 10,
            ];
        }

        // Render the role overview.
        $visibility_roles = $this->rasalasSettings->getSetting('roles');

        $form['association']['role_script'] = [
            '#type' => 'details',
            '#title' => $this->t('Roles'),
            '#group' => 'script_scope',
            '#open' => TRUE,
        ];

        $form['association']['role_script']['rasalas_visibility_roles'] = [
            '#type' => 'radios',
            '#title' => $this->t('Add script for specific roles'),
            '#options' => [
                $this->t('Add to the selected roles only'),
                $this->t('Add to every role except the selected ones'),
            ],
            '#default_value' => $this->rasalasSettings->getSetting('visibility_roles'),
        ];
        $role_options = array_map(['\Drupal\Component\Utility\SafeMarkup', 'checkPlain'], user_role_names());
        $form['association']['role_script']['rasalas_roles'] = [
            '#type' => 'checkboxes',
            '#title' => $this->t('Roles'),
            '#default_value' => !empty($visibility_roles) ? $visibility_roles : [],
            '#options' => $role_options,
            '#description' => $this->t('If none of the roles are selected, all users will see script. If a user has any of the roles checked, that user will see script (or not, depending on the setting above).'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);

        // Trim some text values.
        $form_state->setValue('script_id', trim($form_state->getValue('script_id')));
        $form_state->setValue('rasalas_pages', trim($form_state->getValue('rasalas_pages')));
        $form_state->setValue('rasalas_roles', array_filter($form_state->getValue('rasalas_roles')));

        // Verify that every path is prefixed with a slash.
        if ($form_state->getValue('rasalas_visibility_pages') != 2) {
            $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('rasalas_pages'));
            foreach ($pages as $page) {
                if (strpos($page, '/') !== 0 && $page !== '<front>') {
                    $form_state->setErrorByName(
                        'rasalas_pages',
                        $this->t('Path "@page" not prefixed with slash.', ['@page' => $page])
                    );
                    // Drupal forms show one error only.
                    break;
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $config = $this->config('rasalas.settings');
        $config
            ->set('script_id', $form_state->getValue('script_id'))
            ->set('visibility_pages', $form_state->getValue('rasalas_visibility_pages'))
            ->set('pages', $form_state->getValue('rasalas_pages'))
            ->set('visibility_roles', $form_state->getValue('rasalas_visibility_roles'))
            ->set('roles', $form_state->getValue('rasalas_roles'))
            ->save();

        parent::submitForm($form, $form_state);
    }

}
