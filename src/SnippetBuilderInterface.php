<?php

namespace Drupal\rasalas;

/**
 * Interface SnippetBuilderInterface.
 *
 * @package Drupal\rasalas
 */
interface SnippetBuilderInterface
{
    /**
     * Implements hook_page_attachment().
     */
    public function pageAttachment(array &$attachments);
}
