<?php

namespace Drupal\rasalas;

/**
 * Interface SnippetAccessInterface.
 *
 * @package Drupal\rasalas
 */
interface SnippetAccessInterface
{

    /**
     * Determines whether we add script to page.
     *
     * @return bool
     *   Return TRUE if user can access snippet.
     */
    public function check();

}
