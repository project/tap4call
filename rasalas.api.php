<?php
/**
 * @file
 * Hooks provided by the Rasalas module.
 */

use Drupal\Core\Access\AccessResult;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Control access to a Rasalas script.
 *
 * Modules may implement this hook if they want to disable script for some
 * reasons.
 *
 * @return \Drupal\Core\Access\AccessResultInterface|bool|null
 *   - RASALAS_ACCESS_ALLOW: If script is allowed.
 *   - RASALAS_ACCESS_DENY: If script is disabled.
 *   - RASALAS_ACCESS_IGNORE: If script check is ignored.
 *
 * @ingroup node_access
 */
function hook_rasalas_access()
{
    // Disable for frontpage.
    if (\Drupal::service('path.matcher')->isFrontPage()) {
        return AccessResult::forbidden();
    }
    return AccessResult::neutral();
}

/**
 * Alter results of Rasalas access check results.
 */
function hook_rasalas_access_alter(&$results)
{
    // Force disable for frontpage.
    if (\Drupal::service('path.matcher')->isFrontPage()) {
        $result = AccessResult::forbidden();
    } else {
        $result = AccessResult::neutral();
    }
    $results['my_module_check'] = $result;
}

/**
 * @} End of "addtogroup hooks".
 */
